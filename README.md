# Forum SPA

We created a Front end Forum SPA to test our knowledge of Angular and Bootstrap.


## Trello board



https://trello.com/b/OCADQl7f/forum-project-front-end

##Back-End

https://gitlab.com/dinenov/forumapi


## Installation

  

```bash

$ npm install

```


## Running the app

  

```bash

# in browser

$ ng s -o

## Test


```bash

# unit tests

$ ng test

## Support

  

This is a one off project no continues support wath so ever.


## Built With

Angular,Boostrap, Blood, Sweat and Tears...


## Authors

#### Dimitar Nenov,

  

#### Martin Nedyalkov,

  

#### Mihail Mishev


## Acknowledgments

#### Thanks to Rosen Urkov and Stoyan (Lastname unknown maybe Targaryen:D) for all the help.

