import {
  Component,
  OnInit,
  OnDestroy,
} from '@angular/core';
import {
  PostsDataServices
} from '../../core/services/posts-data.service';
import {
  PostWithComments
} from '../../models/post-with-comments';
import {
  Subscription
} from 'rxjs';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.css']
})
export class PostViewComponent implements OnInit, OnDestroy {
  public post: PostWithComments;
  public routeParamsSubscription: Subscription;
  public postSubscription: Subscription;
  public likeSubscription: Subscription;
  public dislikeSubscription: Subscription;
  public updatePostSubscription: Subscription;
  public flagPostSubscription: Subscription;
  public lockPostSubscription: Subscription;
  public deletePostSubscription: Subscription;
  public title: string;
  public description: string;
  public postToUpdate: string;
  public successMessage: any;
  public lockMessage: any;
  public flagMessage: string;
  public showEditButton: boolean;
  public showDeleteButton: boolean;
  public showLockButton: boolean;
  public isBanned: boolean;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly postsDataServices: PostsDataServices,
    private readonly router: Router,
    private readonly authService: AuthService,
    private modalService: NgbModal,

  ) { }

  ngOnInit() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription = this.postsDataServices.getPost(params.id).subscribe((data: PostWithComments) => {
          this.post = data;

          this.title = data.title;
          this.description = data.description;
          this.postToUpdate = data.body;
          const reversed = this.authService.reverseToken();
          const isAdmin = this.authService.setAdminStatus();
          if (data.author.username === reversed.username || isAdmin === 1) {
            this.showEditButton = true;
            this.showDeleteButton = true;
            this.showLockButton = true;
          }
        },
          (err: any) => {
            if (err.status === 404) {
              this.router.navigate(['/not-found']);
            }
          });
      }
    );
    this.isBanned = this.authService.getBanstatus();
  }

  public likePost() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.likeSubscription = this.postsDataServices.likePost(params.id).subscribe((data: PostWithComments) => {
          this.postSubscription.unsubscribe();
          this.postSubscription = this.postsDataServices.getPost(params.id).subscribe((data2: PostWithComments) => {
            this.post = data2;
          });
        });
      }
    );
  }

  public dislikePost() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.dislikeSubscription = this.postsDataServices.dislikePost(params.id).subscribe((data: PostWithComments) => {
          this.postSubscription.unsubscribe();
          this.postSubscription = this.postsDataServices.getPost(params.id).subscribe((data2: PostWithComments) => {
            this.post = data2;
          });
        });
      }
    );
  }

  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
    this.postSubscription.unsubscribe();
    if (this.likeSubscription) {
      this.likeSubscription.unsubscribe();
    }
    if (this.dislikeSubscription) {
      this.dislikeSubscription.unsubscribe();
    }
    if (this.updatePostSubscription) {
      this.updatePostSubscription.unsubscribe();
    }
    if (this.flagPostSubscription) {
      this.flagPostSubscription.unsubscribe();
    }
    if (this.deletePostSubscription) {
      this.deletePostSubscription.unsubscribe();
    }
    if (this.lockPostSubscription) {
      this.lockPostSubscription.unsubscribe();
    }
  }


  commentCreated() {
    this.routeParamsSubscription.unsubscribe();
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription.unsubscribe();
        this.postSubscription = this.postsDataServices.getPost(params.id).subscribe((data: PostWithComments) => {
          this.post = data;
        });
      }
    );
  }
  open(content) {
    this.modalService.open(content);
  }


  public updatePost(title, description, body) {
    const post = {
      title,
      description,
      body
    };
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription.unsubscribe();
        this.updatePostSubscription = this.postsDataServices.updatePost(params.id, post).subscribe((data) => {
          if (data.message === 'Post has been updated successfully!') {
            this.successMessage = data.message;
          }
          setTimeout(() => {
            location.reload();
          }, 1000);
          this.router.navigate([`/posts/${params.id}`]);
        }, (err: any) => {
          this.successMessage = 'Post is locked from change';
        });
      });
  }

  public deletePost() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription.unsubscribe();
        this.deletePostSubscription = this.postsDataServices.deletePost(params.id).subscribe((data) => {
          // location.reload()
          this.router.navigate(['/posts']);
        }, (err: any) => {
          this.lockMessage = 'Post is locked from change';

        });
      });
  }

  public flagPost(reason: any) {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription.unsubscribe();
        this.flagPostSubscription = this.postsDataServices.flagPost({
          reason
        }, params.id).subscribe((data) => {
          this.flagMessage = 'Send to Admin for review';
          this.router.navigate([`/posts/${params.id}`]);
        });
      });
  }

  public lockPost() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription.unsubscribe();

        this.flagPostSubscription = this.postsDataServices.lockPost(params.id).subscribe((data) => {
          this.lockMessage = 'Post locked successfully!';
          setTimeout(() => {
            location.reload();
          }, 1500);
          this.router.navigate([`/posts/${params.id}`]);
        }, (err: any) => {
          this.lockMessage = 'Post is already locked';
          setTimeout(() => {
            location.reload();
          }, 1000);
        });
      });
  }
}
