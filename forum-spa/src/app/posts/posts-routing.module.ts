import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PostsComponent } from './posts/posts.component';
import { PostViewComponent } from './post-view/post-view.component';

const routes: Routes = [
  { path: '', component: PostsComponent, pathMatch: 'full' },
  { path: ':id', component: PostViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
