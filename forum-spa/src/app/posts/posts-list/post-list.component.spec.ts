import { TestBed, async } from '@angular/core/testing';
import { PostsDataServices } from 'src/app/core/services/posts-data.service';
import { PostsListComponent } from './posts-list.component';
import { PostViewComponent } from '../post-view/post-view.component';
import { PostsComponent } from '../posts/posts.component';
import { PostsRoutingModule } from '../posts-routing.module';
import { AppModule } from 'src/app/app.module';
import { VoteComponent } from 'src/app/shared/vote/vote.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PostsModule } from '../posts.module';
import { CoreModule } from 'src/app/core/core.module';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { of } from 'rxjs';
import { PostNoComments } from 'src/app/models/posts-no-comments';

describe('PostsListComponent', () => {
    let fixture;
    //let activatedRoute=jasmine.createSpyObj('ActivatedRoute',['params'])
    let postsDataServices=jasmine.createSpyObj('PostsDataServices',['getAllPosts','likePost','getPost','dislikePost','updatePost','deletePost','flagPost','lockPost'])
   
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PostsListComponent, PostsComponent, PostViewComponent
                
            ],
            imports: [
                PostsRoutingModule,
        NgbModule,
        HttpClientModule,
        SharedModule,
        CommonModule,
        NgxPaginationModule
                
            ],
            providers: [
                {
                    provide: PostsDataServices,
                    useValue: postsDataServices
                },
                

            ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });
    it('should create the component', () => {
        fixture = TestBed.createComponent(PostsListComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    });
    describe('OnInit', () => {
        it('should initialize with the correct post data', async () => {
            const posts:PostNoComments[]=[{
                id: '1',
                title: '1',
                description: '1',
                body: '1',
                author: {
                    id: 'string',
                    username: 'string',
                    banstatus: 0,
                    roles: [],
                    areFriends: true,
                    isDeleted: false,
                  },
                datePosted: new Date,
                dateModified: new Date,
                isLocked:false,
                like: [],
                numberOfLikes: 1,
                dislike: [],
                numberOfDislikes: 1,
                flag: [],
                numberOfComments:1,
                comments: [],
            },
            {
                id: '2',
                title: '1',
                description: '1',
                body: '1',
                author: {
                    id: 'string1',
                    username: 'string1',
                    banstatus: 0,
                    roles: [],
                    areFriends: true,
                    isDeleted: false,
                  },
                datePosted: new Date,
                dateModified: new Date,
                isLocked:false,
                like: [],
                numberOfLikes: 1,
                dislike: [],
                numberOfDislikes: 1,
                flag: [],
                numberOfComments:1,
                comments: [],
            }]
            fixture = TestBed.createComponent(PostsListComponent);
            const component = fixture.debugElement.componentInstance
            postsDataServices.getAllPosts.and.returnValue(of(posts));

            await fixture.detectChanges();

            expect(component.posts).toEqual(posts)

        })







    })


})