import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { PostsDataServices } from '../../core/services/posts-data.service';
import { PostNoComments } from '../../models/posts-no-comments';
import { CommentsDataService } from 'src/app/core/services/comments-data.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit, OnDestroy {
  @Output()
  public selectedPost = new EventEmitter<string>();
  public posts: PostNoComments[] = [];
  private postsSubscription: Subscription;

  constructor(
    private readonly postsDataService: PostsDataServices,
  ) { }

  ngOnInit() {
    this.postsSubscription = this.postsDataService.getAllPosts().subscribe(
      (posts: PostNoComments[]) => {
        this.posts = posts;

        this.posts.forEach((post: PostNoComments, index: number) => {
          this.posts[index].numberOfLikes = post.like.length;
          post.numberOfDislikes = post.dislike.length;
          post.numberOfComments = post.comments.length;
        });
        this.posts.sort((a, b) => {
          const c = new Date(a.datePosted).valueOf();
          const d = new Date(b.datePosted).valueOf();
          return d - c;
        });
      }
    );
  }

  selectPost(post: PostNoComments) {
    this.selectedPost.emit(post.id);
  }

  ngOnDestroy() {
    if (this.postsSubscription) {
      this.postsSubscription.unsubscribe();
    }
  }

}
