import { NgModule } from '@angular/core';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostsComponent } from './posts/posts.component';
import { PostsRoutingModule } from './posts-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { PostViewComponent } from './post-view/post-view.component';
import { CoreModule } from '../core/core.module';
import { RouterModule } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  providers: [],
  declarations: [PostsListComponent, PostsComponent, PostViewComponent],
  imports: [
    PostsRoutingModule,
    NgbModule,
    HttpClientModule,
    SharedModule,
    CommonModule,
    NgxPaginationModule
  ],
})
export class PostsModule { }
