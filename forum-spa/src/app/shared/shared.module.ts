import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoteComponent } from './vote/vote.component';
import { ViewCommentsComponent } from './comments/view-comments/view-comments.component';
import { CreateCommentsComponent } from './comments/create-comments/create-comments.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ActivityComponent } from './activity/activity.component';
import { FriendsButtonsComponent } from './friends-buttons/friends-buttons.component';
import { AdminButtonsComponent } from './admin-buttons/admin-buttons.component';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [
    VoteComponent,
    ViewCommentsComponent,
    CreateCommentsComponent,
    NotFoundComponent,
    ActivityComponent,
    FriendsButtonsComponent,
    AdminButtonsComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    VoteComponent,
    ViewCommentsComponent,
    CommonModule,
    CreateCommentsComponent,
    NotFoundComponent,
    ActivityComponent,
    FriendsButtonsComponent,
    AdminButtonsComponent,
    ErrorComponent
  ]
})
export class SharedModule { }
