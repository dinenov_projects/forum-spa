import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PostNoComments } from 'src/app/models/posts-no-comments';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  @Input()
  public posts: any;
  @Output()
  public selectedPost = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  selectPost(post: PostNoComments) {
    this.selectedPost.emit(post.id);
  }
}
