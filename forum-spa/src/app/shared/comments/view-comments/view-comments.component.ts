import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { PostWithComments } from '../../../models/post-with-comments';
import { Router } from '@angular/router';

import { PostsDataServices } from '../../../core/services/posts-data.service';
import { CommentsDataService } from '../../../core/services/comments-data.service';
import { AuthService } from '../../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-comments',
  templateUrl: './view-comments.component.html',
  styleUrls: ['./view-comments.component.css']
})
export class ViewCommentsComponent implements OnInit, OnDestroy {
  @Input()
  post: PostWithComments;
  @Output()
  deletedCommentEvent = new EventEmitter();
  @Output()
  editedCommentEvent = new EventEmitter();
  public routeParamsSubscription: Subscription;
  public commentSubscription: Subscription;
  public deleteCommentSubscription: Subscription;
  public editCommentSubscription: Subscription;
  public postSubscription: Subscription;
  public showDeleteCommentButton: boolean;
  public showEditCommentButton: boolean;
  public commentToUpdate: string;
  public successMessage: any;
  public isBanned: boolean;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly postsDataServices: PostsDataServices,
    private readonly commentsDataServices: CommentsDataService,
    private readonly router: Router,
    private readonly authService: AuthService,

    private modalService: NgbModal,
  ) { }




  ngOnInit() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postSubscription = this.postsDataServices.getPost(params.id).subscribe((data: PostWithComments) => {
          // this.post = data;
        },
          (err: any) => {
            if (err.status === 404) {
              this.router.navigate(['/not-found']);
            }
          });
      }
    );
    this.isBanned = this.authService.getBanstatus();
  }


  public showButtons(authorId: string) {
    const reversed = this.authService.reverseToken();
    const isAdmin = this.authService.setAdminStatus();
    if (authorId === reversed.id || isAdmin === 1) {
      return true;
    }
    return false;
  }

  selectUser(userId: string) {
    this.router.navigate(['/users/', userId]);
  }

  deleteComment(commentId: string) {
    this.deleteCommentSubscription = this.commentsDataServices.deleteComment(commentId).subscribe((data) => {
      this.deletedCommentEvent.emit();
    });
  }

  open(content) {
    this.modalService.open(content);
  }

  editComment(commentId: string, commentBody) {
    if (commentBody.length >= 1) {
      this.editCommentSubscription = this.commentsDataServices.editComment(commentId, commentBody).subscribe((data) => {
        this.post = data;
        this.commentToUpdate = data.commentBody;
        if (data.message === 'Comment has been updated successfully!') {
          this.successMessage = data.message;
        }
        this.editedCommentEvent.emit();
        this.modalService.dismissAll();
      });
    }
  }
  ngOnDestroy() {
    if(this.routeParamsSubscription) {
      this.routeParamsSubscription.unsubscribe();
    }
    if(this.commentSubscription) {
      this.commentSubscription.unsubscribe();
    }
    if(this.deleteCommentSubscription) {
      this.deleteCommentSubscription.unsubscribe();
    }
    if(this.editCommentSubscription) {
      this.editCommentSubscription.unsubscribe();
    }
    if(this.postSubscription) {
      this.postSubscription.unsubscribe();
    }
  }

}
