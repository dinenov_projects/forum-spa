import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShowUser } from 'src/app/models/show-user';

@Component({
  selector: 'app-friends-buttons',
  templateUrl: './friends-buttons.component.html',
  styleUrls: ['./friends-buttons.component.css']
})
export class FriendsButtonsComponent implements OnInit {
  @Input()
  user: ShowUser;
  @Input()
  hasRequest: boolean;
  @Input()
  hasSentRequest: boolean;
  @Output()
  followUserEvent = new EventEmitter();
  @Output()
  unfollowUserEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  followUser() {
    this.followUserEvent.emit();
  }

  unfollowUser() {
    this.unfollowUserEvent.emit();
  }
}
