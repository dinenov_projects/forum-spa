import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ShowUser } from '../../models/show-user';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit, OnChanges {
  @Input()
  component: {id: string, like: any[], dislike: any[], numberOfLikes: number, numberOfDislikes: number};
  @Output()
  likeEvent = new EventEmitter<any>();
  @Output()
  dislikeEvent = new EventEmitter<any>();

  constructor(
    private readonly storageService: StorageService,
  ) { }

  public hasLiked(): string {
    const userId: string = this.storageService.get('userId');
    let hasLiked =  false;
    this.component.like.forEach((user: ShowUser) => {
      if (user.id.toString() === userId) {
        hasLiked = true;
      }
    });
    if (hasLiked) {
      return 'oi oi-thumb-up text-success';
    }
    return 'oi oi-thumb-up text-secondary';
  }

  public hasDisliked(): string {
    const userId: string = this.storageService.get('userId');
    let hasDisliked =  false;
    this.component.dislike.forEach((user: ShowUser) => {
      if (user.id.toString() === userId) {
        hasDisliked = true;
      }
    });
    if (hasDisliked) {
      return 'oi oi-thumb-down text-danger';
    }
    return 'oi oi-thumb-down text-secondary';
  }

  public numberOfVotes() {
    this.component.numberOfLikes = this.component.like.length;
    this.component.numberOfDislikes = this.component.dislike.length;
  }

  public thumbUp() {
    this.likeEvent.emit();
  }

  public thumbDown() {
    this.dislikeEvent.emit();
  }

  ngOnInit() {
    this.numberOfVotes();
  }

  ngOnChanges() {
    this.numberOfVotes();
  }

}
