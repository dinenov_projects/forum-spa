export class CreatePostModel {
    
    readonly title: string;
    
    readonly description: string;
    
    readonly body: string;
}