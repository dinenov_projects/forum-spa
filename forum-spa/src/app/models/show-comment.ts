import { ShowUser } from './show-user';

export interface ShowComment {
  id: string;
  body: string;
  author: ShowUser;
  dateCreated: Date;
  dateModified: Date;
}
