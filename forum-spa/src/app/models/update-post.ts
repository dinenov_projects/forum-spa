export class UpdatePostModel {
    
    readonly title: string;
    
    readonly description: string;
    
    readonly body: string;
}