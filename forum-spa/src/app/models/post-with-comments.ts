import { ShowUser } from './show-user';
import { ShowComment } from './show-comment';

export interface PostWithComments {
  id: string;
  title: string;
  description: string;
  body: string;
  comments: ShowComment[];
  author: ShowUser;
  datePosted: Date;
  dateModified: Date;
  isLocked: boolean;
  like: ShowUser[];
  numberOfLikes: number;
  dislike: ShowUser[];
  numberOfDislikes: number;
  flag: any[];
}
