export interface ShowUser {
  id: string;
  username: string;
  banstatus: any;
  roles: any[];
  areFriends: boolean;
  isDeleted: boolean;
}
