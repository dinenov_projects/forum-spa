import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { TestBed } from '@angular/core/testing';
import { AuthService } from '../../core/services/auth.service';
import { LoginComponent } from './login.component';
import { Router } from '@angular/router';
import { SharedModule } from '../../../app/shared/shared.module';
import { FormsModule, NgModel } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { PostsRoutingModule } from 'src/app/posts/posts-routing.module';
describe('LoginComponent', () => {
    let fixture;
    const authService = jasmine.createSpyObj('AuthService', ['login']);
    const router = jasmine.createSpyObj('Router', ['navigate']);
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: AuthService,
                useValue: authService,
            },
            {
                provide: Router ,
                useValue: router,
            },
        ],imports: [
            SharedModule,
            FormsModule,
          ],
          declarations: [LoginComponent]
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
          (fixture.nativeElement as HTMLElement).remove();
        }
      });

     it('should be created', () => {
       fixture = TestBed.createComponent(LoginComponent);
       const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();

     });
      it('login should call authLogin', async() => {
        const username="testUser"
        const password="testPassword"
        const user ={username,password}
        
        fixture = TestBed.createComponent(LoginComponent);
        const app = fixture.debugElement.componentInstance;
        authService.login.and.returnValue(of(user));
        app.login(username,password)
        await fixture.detectChanges();
        expect(authService.login).toHaveBeenCalledTimes(1)
      });
      
      it('login should call refer to /posts on succses', async() => {
        const username="testUser"
        const password="testPassword"
        const user ={username,password}
        
        fixture = TestBed.createComponent(LoginComponent);
        const app = fixture.debugElement.componentInstance;
        authService.login.and.returnValue(of(user));
        const route = ['/posts'];
        router.navigate.calls.reset();
        app.login(username,password)
        await fixture.detectChanges();
        expect(router.navigate).toHaveBeenCalledTimes(1)
        expect(router.navigate).toHaveBeenCalledWith(route);
      });
      


})
