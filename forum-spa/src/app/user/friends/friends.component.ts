import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShowUser } from 'src/app/models/show-user';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UsersDataService } from 'src/app/core/services/users-data.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit, OnDestroy {
  public friends: ShowUser[];
  public requests: ShowUser[];
  public friendsSubscription: Subscription;
  public toggled = false;

  constructor(
    private readonly router: Router,
    private readonly usersDataService: UsersDataService
  ) { }

  ngOnInit() {
    this.friendsSubscription = this.usersDataService.getFriends().subscribe((friends) => {
      if (friends === null) {
        return;
      }
      const areFriends = [];
      friends.followers.forEach((follower) => {
        if (friends.following.find((following) => follower.id === following.id)) {
          areFriends.push(follower);
        }
      });
      this.friends = areFriends;
      const requests = [];
      friends.followers.forEach((follower) => {
        if (!friends.following.find((following) => follower.id === following.id)) {
          requests.push(follower);
        }
      });
      this.requests = requests;
    });
  }

  public redirectToUser(userId: string): void {
    this.router.navigate(['/users', userId]);
  }

  public toggle() {
    this.toggled = !this.toggled;
  }

  ngOnDestroy() {
    this.friendsSubscription.unsubscribe();
  }

}
