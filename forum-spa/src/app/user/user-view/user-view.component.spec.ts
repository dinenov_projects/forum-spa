import { TestBed, async } from '@angular/core/testing';
import { UsersComponent } from '../users/users.component';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersDataService } from '../../core/services/users-data.service';
import { UsersListComponent } from '../users-list/users-list.component';
import { UserViewComponent } from '../user-view/user-view.component';
import { FriendsComponent } from '../friends/friends.component';
import { FriendsListComponent } from '../friends-list/friends-list.component';
import { FriendsRequestsListComponent } from '../friends-requests-list/friends-requests-list.component';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from '../user-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { of } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { ShowUser } from 'src/app/models/show-user';
import { HttpErrorResponse } from '@angular/common/http';
import { asyncError } from '../../../testing/async-observable-helpers';

describe('UserViewComponent', () => {
    let fixture;
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const usersDataService = jasmine.createSpyObj(
        'UsersDataService',
        [
            'getUser',
            'areFriends',
            'hasFriendsRequest',
            'hasSentFriendsRequest',
            'getUserActivity',
            'followUser',
            'unfollowUser',
            'deleteUser',
            'banUser',
            'unbanUser'
        ]);
    const authService = jasmine.createSpyObj('AuthService', ['setAdminStatus', 'reverseToken', 'getBanstatus']);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                UsersListComponent,
                UsersComponent,
                UserViewComponent,
                FriendsComponent,
                FriendsListComponent,
                FriendsRequestsListComponent
            ],
            imports: [
                CommonModule,
                UserRoutingModule,
                SharedModule
            ],
            providers: [
                {
                    provide: Router,
                    useValue: router
                },
                {
                    provide: UsersDataService,
                    useValue: usersDataService
                },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of({ id: 1 })
                    }
                },
                {
                    provide: AuthService,
                    useValue: authService
                }

            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(UserViewComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    });

    describe('OnInit', () => {
        it('should initialize with the correct user data', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'User' }],
                areFriends: false,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            authService.reverseToken.and.returnValue({ id: '1' });
            authService.setAdminStatus.and.returnValue(false);
            usersDataService.getUser.and.returnValue(of(user));
            usersDataService.areFriends.and.returnValue(of(true));
            usersDataService.hasFriendsRequest.and.returnValue(of(false));
            usersDataService.hasSentFriendsRequest.and.returnValue(of(false));
            usersDataService.getUserActivity.and.returnValue(of({ posts: [] }));

            await fixture.detectChanges();

            expect(component.user).toEqual(user);
        });

        it('should not initialize activity data', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'User' }],
                areFriends: false,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            authService.reverseToken.and.returnValue({ id: '2' });
            authService.setAdminStatus.and.returnValue(false);
            usersDataService.getUser.and.returnValue(of(user));
            usersDataService.areFriends.and.returnValue(of(true));
            usersDataService.hasFriendsRequest.and.returnValue(of(false));
            usersDataService.hasSentFriendsRequest.and.returnValue(of(false));
            usersDataService.getUserActivity.and.returnValue(of({ posts: [] }));

            await fixture.detectChanges();

            expect(component.user).toEqual(user);
        });

        it('should redirect to not-found when 404 code was received', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: false,
            };
            const errorResponse = new HttpErrorResponse({
                error: 'test 404 error',
                status: 404, statusText: 'Not Found'
            });
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            authService.reverseToken.and.returnValue({ id: '1' });
            authService.setAdminStatus.and.returnValue(false);
            authService.reverseToken.and.returnValue(true);
            usersDataService.getUser.and.returnValue(asyncError(errorResponse));
            usersDataService.areFriends.and.returnValue(of(false));
            usersDataService.hasFriendsRequest.and.returnValue(of(true));
            usersDataService.hasSentFriendsRequest.and.returnValue(of(false));
            router.navigate.calls.reset();

            await fixture.detectChanges();

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/not-found']);
        });

        it('should redirect to error when another http status code was received', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{}],
                areFriends: false,
            };
            const errorResponse = new HttpErrorResponse({
                error: 'test 400 error',
                status: 400, statusText: 'Error'
            });
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            authService.reverseToken.and.returnValue({ id: '2' });
            authService.setAdminStatus.and.returnValue(true);
            usersDataService.getUser.and.returnValue(asyncError(errorResponse));
            usersDataService.areFriends.and.returnValue(of(false));
            usersDataService.hasFriendsRequest.and.returnValue(of(false));
            usersDataService.hasSentFriendsRequest.and.returnValue(of(true));
            router.navigate.calls.reset();

            await fixture.detectChanges();

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/error']);
        });
    });

    describe('followUser', () => {
        it('followUser should call usersDataService.followUser with the correct id', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            usersDataService.followUser.and.returnValue(of(user));
            usersDataService.followUser.calls.reset();

            await fixture.detectChanges();
            component.followUser();

            expect(usersDataService.followUser).toHaveBeenCalledTimes(1);
            expect(usersDataService.followUser).toHaveBeenCalledWith(user.id);
        });
    });

    describe('unfollowUser', () => {
        it('unfollowUser should call usersDataService.unfollowUser with the correct id', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            usersDataService.unfollowUser.and.returnValue(of(user));
            usersDataService.unfollowUser.calls.reset();

            await fixture.detectChanges();
            component.unfollowUser();

            expect(usersDataService.unfollowUser).toHaveBeenCalledTimes(1);
            expect(usersDataService.unfollowUser).toHaveBeenCalledWith(user.id);
        });
    });

    describe('deleteUser', () => {
        it('deleteUser should call usersDataService.deleteUser with the correct id', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;

            await fixture.detectChanges();
            usersDataService.deleteUser.and.returnValue(of(user));
            usersDataService.deleteUser.calls.reset();

            component.deleteUser();

            expect(usersDataService.deleteUser).toHaveBeenCalledTimes(1);
            expect(usersDataService.deleteUser).toHaveBeenCalledWith(user.id);
        });


        it('deleteUser should call router.navigate with [`/users`]', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            usersDataService.deleteUser.and.returnValue(of(user));

            await fixture.detectChanges();
            router.navigate.calls.reset();

            component.deleteUser();

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/users']);
        });
    });

    describe('banUser', () => {
        it('banUser should call usersDataService.banUser with the correct id and description', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;

            await fixture.detectChanges();
            usersDataService.banUser.and.returnValue(of(user));
            usersDataService.banUser.calls.reset();

            component.banUser('reason');

            expect(usersDataService.banUser).toHaveBeenCalledTimes(1);
            expect(usersDataService.banUser).toHaveBeenCalledWith(user.id, { description: 'reason' });
        });
    });

    describe('unbanUser', () => {
        it('unbanUser should call usersDataService.unbanUser with the correct id', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;

            await fixture.detectChanges();
            usersDataService.unbanUser.and.returnValue(of(user));
            usersDataService.unbanUser.calls.reset();

            component.unbanUser();

            expect(usersDataService.unbanUser).toHaveBeenCalledTimes(1);
            expect(usersDataService.unbanUser).toHaveBeenCalledWith(user.id);
        });
    });

    describe('selectPost', () => {
        it('selectPost should navigate to the chosen post', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            const id = '1';
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;

            await fixture.detectChanges();
            router.navigate.calls.reset();

            component.selectPost(id);

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith([`/posts/${id}`]);
        });
    });

    describe('isTheSamePerson', () => {
        it('isTheSamePerson should return true if the logged in user is viewing their own profile', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            const id = '1';
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            authService.reverseToken.and.returnValue({ id: '1' });

            await fixture.detectChanges();

            const result = component.isTheSamePerson(id);

            expect(result).toBe(true);
        });

        it('isTheSamePerson should return false if the logged in user is viewing not their own profile', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            const id = '1';
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;
            authService.reverseToken.and.returnValue({ id: '1' });

            await fixture.detectChanges();

            const result = component.isTheSamePerson(id);

            expect(result).toBe(true);
        });

        it('isTheSamePerson should call authService.reverseToken', async () => {
            const user: ShowUser = {
                id: '1',
                isDeleted: false,
                username: 'username',
                banstatus: {},
                roles: [{ name: 'Admin' }],
                areFriends: undefined,
            };
            const id = '1';
            fixture = TestBed.createComponent(UserViewComponent);
            const component = fixture.debugElement.componentInstance;
            component.user = user;

            await fixture.detectChanges();
            authService.reverseToken.calls.reset();

            component.isTheSamePerson(id);

            expect(authService.reverseToken).toHaveBeenCalledTimes(1);
        });
    });
});
