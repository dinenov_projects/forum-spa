import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShowUser } from 'src/app/models/show-user';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersDataService } from '../../core/services/users-data.service';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit, OnDestroy {
  public user: ShowUser;
  public hasRequest: boolean;
  public hasSentRequest: boolean;
  public isBanned: boolean;
  public userSubscription: Subscription;
  public friendsSubscription: Subscription;
  public routeParamsSubscription: Subscription;
  public hasFriendsRequestSubscription: Subscription;
  public hasSentFriendsRequestSubscription: Subscription;
  public unfollowUserSubscription: Subscription;
  public followUserSubscription: Subscription;
  public deleteUserSubscription: Subscription;
  public banUserSubscription: Subscription;
  public unbanUserSubscription: Subscription;
  public activitySubscription: Subscription;
  public posts;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly usersDataService: UsersDataService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.initData();
    this.isBanned = this.authService.getBanstatus();
  }

  public isAdmin(roles: { name: string }[]): boolean {
    let isAdmin = false;
    roles.forEach((role) => {
      if (role.name === 'Admin') {
        isAdmin = true;
      }
    });
    return isAdmin;
  }

  public followUser(): void {
    this.followUserSubscription = this.usersDataService.followUser(this.user.id).subscribe(
      res => {
        this.unsubscribeData();
        this.initData();
      }
    );
  }

  public unfollowUser(): void {
    this.unfollowUserSubscription = this.usersDataService.unfollowUser(this.user.id).subscribe(
      res => {
        this.unsubscribeData();
        this.initData();
      }
    );
  }

  ngOnDestroy() {
    if (this.routeParamsSubscription) {
      this.routeParamsSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
    this.unsubscribeData();
  }

  private initData() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.userSubscription = this.usersDataService.getUser(params.id).subscribe(
          (user: ShowUser) => {
            this.user = user;
            this.getActivity(params.id);
          },
          (err: any) => {
            if (err.status === 404) {
              this.router.navigate(['/not-found']);
            } else {
              this.router.navigate(['/error']);
            }
          }
        );
        this.friendsSubscription = this.usersDataService.areFriends(params.id).subscribe(
          (areFriends: boolean) => {
            this.user.areFriends = areFriends;
          }
        );
        this.hasFriendsRequestSubscription = this.usersDataService.hasFriendsRequest(params.id).subscribe(
          (hasRequest: boolean) => {
            this.hasRequest = hasRequest;
          }
        );
        this.hasSentFriendsRequestSubscription = this.usersDataService.hasSentFriendsRequest(params.id).subscribe(
          (hasSentRequest: boolean) => {
            this.hasSentRequest = hasSentRequest;
          }
        );
      }
    );
  }

  private unsubscribeData() {
    if (this.friendsSubscription) {
      this.friendsSubscription.unsubscribe();
    }
    if (this.hasFriendsRequestSubscription) {
      this.hasFriendsRequestSubscription.unsubscribe();
    }
    if (this.hasSentFriendsRequestSubscription) {
      this.hasSentFriendsRequestSubscription.unsubscribe();
    }
    if (this.followUserSubscription) {
      this.followUserSubscription.unsubscribe();
    }
    if (this.unfollowUserSubscription) {
      this.unfollowUserSubscription.unsubscribe();
    }
    if (this.deleteUserSubscription) {
      this.deleteUserSubscription.unsubscribe();
    }
    if (this.banUserSubscription) {
      this.banUserSubscription.unsubscribe();
    }
    if (this.unbanUserSubscription) {
      this.unbanUserSubscription.unsubscribe();
    }
  }

  public setAdminStatus(): number {
    return this.authService.setAdminStatus();
  }

  public deleteUser() {
    this.deleteUserSubscription = this.usersDataService.deleteUser(this.user.id).subscribe(
      res => {
        this.router.navigate(['/users']);
      }
    );
  }

  public banUser(description: string) {
    this.banUserSubscription = this.usersDataService.banUser(this.user.id, { description }).subscribe(
      res => {
        this.unsubscribeData();
        this.initData();
      }
    );
  }

  public unbanUser() {
    this.unbanUserSubscription = this.usersDataService.unbanUser(this.user.id).subscribe(
      res => {
        this.unsubscribeData();
        this.initData();
      }
    );
  }

  public getActivity(userId: string) {
    if (this.user.areFriends || this.setAdminStatus()  || this.isTheSamePerson()) {
      this.activitySubscription = this.usersDataService.getUserActivity(userId).subscribe(
        res => {
          this.posts = res.posts.slice(0, 5);
        }
      );
    }
  }

  public selectPost(postId: string) {
    this.router.navigate([`/posts/${postId}`]);
  }

  public isTheSamePerson(): boolean {
    const reversedToken = this.authService.reverseToken();
    if (this.user.id === reversedToken.id) {
      return true;
    }
    return false;
  }

}
