import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShowUser } from 'src/app/models/show-user';

@Component({
  selector: 'app-friends-requests-list',
  templateUrl: './friends-requests-list.component.html',
  styleUrls: ['./friends-requests-list.component.css']
})
export class FriendsRequestsListComponent implements OnInit {
  @Input()
  public requests: ShowUser[];
  @Output()
  public selectedUser = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  public selectUser(user: ShowUser) {
    this.selectedUser.emit(user.id);
  }
}
