import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShowUser } from '../../models/show-user';

@Component({
  selector: 'app-friends-list',
  templateUrl: './friends-list.component.html',
  styleUrls: ['./friends-list.component.css']
})
export class FriendsListComponent implements OnInit {
  @Input()
  public friends: ShowUser[];
  @Output()
  public selectedUser = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {}

  public selectUser(user: ShowUser) {
    this.selectedUser.emit(user.id);
  }
}
