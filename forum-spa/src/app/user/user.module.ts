import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import { UserRoutingModule } from './user-routing.module';
import { UsersComponent } from './users/users.component';
import { UserViewComponent } from './user-view/user-view.component';
import { FriendsComponent } from './friends/friends.component';
import { FriendsListComponent } from './friends-list/friends-list.component';
import { SharedModule } from '../shared/shared.module';
import { FriendsRequestsListComponent } from './friends-requests-list/friends-requests-list.component';

@NgModule({
  declarations: [
    UsersListComponent,
    UsersComponent,
    UserViewComponent,
    FriendsComponent,
    FriendsListComponent,
    FriendsRequestsListComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ],
  exports: [FriendsComponent, FriendsListComponent],
})
export class UserModule { }
