import { TestBed, async } from '@angular/core/testing';
import { UsersComponent } from './users.component';
import { Router } from '@angular/router';
import { UsersDataService } from '../../core/services/users-data.service';
import { UsersListComponent } from '../users-list/users-list.component';
import { UserViewComponent } from '../user-view/user-view.component';
import { FriendsComponent } from '../friends/friends.component';
import { FriendsListComponent } from '../friends-list/friends-list.component';
import { FriendsRequestsListComponent } from '../friends-requests-list/friends-requests-list.component';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from '../user-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { of } from 'rxjs';
import { ShowUser } from '../../models/show-user';
import { HttpErrorResponse } from '@angular/common/http';
import { asyncError } from '../../../testing/async-observable-helpers';

describe('UsersComponent', () => {
    let fixture;
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const usersDataService = jasmine.createSpyObj('UsersDataService', ['getAllUsers', 'areFriends']);
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                UsersListComponent,
                UsersComponent,
                UserViewComponent,
                FriendsComponent,
                FriendsListComponent,
                FriendsRequestsListComponent
            ],
            imports: [
                CommonModule,
                UserRoutingModule,
                SharedModule
            ],
            providers: [
                {
                    provide: Router,
                    useValue: router
                },
                {
                    provide: UsersDataService,
                    useValue: usersDataService
                },

            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(UsersComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    });

    describe('OnInit', () => {
        it('should initialize with the correct users data', async () => {
            const users: ShowUser[] = [
                {
                    id: '1',
                    isDeleted: false,
                    username: 'username',
                    banstatus: {},
                    roles: [{}],
                    areFriends: undefined,
                },
                {
                    id: '2',
                    isDeleted: false,
                    username: 'username2',
                    banstatus: {},
                    roles: [{}],
                    areFriends: undefined,
                }
            ];
            fixture = TestBed.createComponent(UsersComponent);
            const component = fixture.debugElement.componentInstance;
            usersDataService.areFriends.and.returnValue(of(true));
            usersDataService.getAllUsers.and.returnValue(of(
                users
            ));

            await fixture.detectChanges();
            expect(component.users).toEqual(users);
        });

        it('should initialize with the correctly ordered users data', async () => {
            const users: ShowUser[] = [
                {
                    id: '1',
                    isDeleted: false,
                    username: 'username2',
                    banstatus: {},
                    roles: [{}],
                    areFriends: undefined,
                },
                {
                    id: '2',
                    isDeleted: false,
                    username: 'username',
                    banstatus: {},
                    roles: [{}],
                    areFriends: undefined,
                }
            ];
            fixture = TestBed.createComponent(UsersComponent);
            const component = fixture.debugElement.componentInstance;
            usersDataService.areFriends.and.returnValue(of(true));
            usersDataService.getAllUsers.and.returnValue(of(
                users
            ));

            await fixture.detectChanges();
            expect(component.users).toEqual(users);
        });

        it('should redirect to not-found, when 404 code is received', async () => {
            const errorResponse = new HttpErrorResponse({
                error: 'test 404 error',
                status: 404, statusText: 'Not Found'
            });
            const route = ['/not-found'];

            fixture = TestBed.createComponent(UsersComponent);
            usersDataService.areFriends.and.returnValue(of(true));
            usersDataService.getAllUsers.and.returnValue(asyncError(errorResponse));
            router.navigate.calls.reset();

            await fixture.detectChanges();

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(route);
        });

        it('should not redirect to not-found, when another http error code is received', async () => {
            const errorResponse = new HttpErrorResponse({
                error: 'test 400 error',
                status: 400, statusText: 'Error'
            });
            const route = ['/error'];

            fixture = TestBed.createComponent(UsersComponent);
            usersDataService.areFriends.and.returnValue(of(true));
            usersDataService.getAllUsers.and.returnValue(asyncError(errorResponse));
            router.navigate.calls.reset();

            await fixture.detectChanges();

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(route);
        });
    });

    describe('redirectToUser', () => {
        it('redirectToUser should redirect to the selected user', () => {
            const route = '/users';
            const userId = '1';
            fixture = TestBed.createComponent(UsersComponent);
            const component = fixture.debugElement.componentInstance;
            router.navigate.calls.reset();

            component.redirectToUser(userId);

            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith([route, userId]);
        });
    });
});
