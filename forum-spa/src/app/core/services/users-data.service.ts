import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ShowUser } from '../../models/show-user';

@Injectable({
  providedIn: 'root'
})
export class UsersDataService {
  public constructor(private readonly http: HttpClient) { }

  public getAllUsers(): Observable<ShowUser[]> {
    return this.http.get<ShowUser[]>(`http://localhost:3000/users`);
  }

  public getUser(userId: string): Observable<ShowUser> {
    return this.http.get<ShowUser>(`http://localhost:3000/users/${userId}`);
  }

  public deleteUser(userId: string): Observable<ShowUser> {
    return this.http.delete<ShowUser>(`http://localhost:3000/users/${userId}`);
  }

  public banUser(userId: string, description: {description: string}): Observable<ShowUser> {
    return this.http.put<ShowUser>(`http://localhost:3000/users/${userId}/banstatus`, description);
  }

  public unbanUser(userId: string): Observable<ShowUser> {
    return this.http.delete<ShowUser>(`http://localhost:3000/users/${userId}/banstatus`);
  }

  public getFriends(): Observable<{followers: ShowUser[], following: ShowUser[]}> {
    return this.http.get<{followers: ShowUser[], following: ShowUser[]}>(`http://localhost:3000/friends`);
  }

  public followUser(userId: string): Observable<{message: string}> {
    return this.http.post<{message: string}>(`http://localhost:3000/users/${userId}/follow`, '');
  }

  public unfollowUser(userId: string): Observable<{message: string}> {
    return this.http.delete<{message: string}>(`http://localhost:3000/users/${userId}/follow`);
  }

  public areFriends(userId: string): Observable<boolean> {
    return this.http.get<boolean>(`http://localhost:3000/users/${userId}/friends`);
  }

  public hasFriendsRequest(userId: string): Observable<boolean> {
    return this.http.get<boolean>(`http://localhost:3000/users/${userId}/friends/requesting`);
  }

  public hasSentFriendsRequest(userId: string): Observable<boolean> {
    return this.http.get<boolean>(`http://localhost:3000/users/${userId}/friends/requested`);
  }

  public getUserActivity(userId: string): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/users/${userId}/activity`);
  }
}
