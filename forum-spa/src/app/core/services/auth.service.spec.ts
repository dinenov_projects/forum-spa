import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { UserRegister } from 'src/app/models/user-register';
import { JWTService } from './jwt.service';
import { UserLogin } from 'src/app/models/login-user';

describe('AuthService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);
    const jwtHelper = jasmine.createSpyObj('JwtHelperService', ['isTokenExpired']);
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const jwtService = jasmine.createSpyObj('JWTService', ['JWTDecoder']);

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
            {
                provide: StorageService,
                useValue: storage,
            },
            {
                provide: JwtHelperService,
                useValue: jwtHelper,
            },
            {
                provide: Router,
                useValue: router,
            },
            {
                provide: JWTService,
                useValue: jwtService
            }
        ]
    }));

    it('should be created', () => {
        const service: AuthService = TestBed.get(AuthService);

        expect(service).toBeTruthy();
    });

    describe('register', () => {
        it('register should register the user', () => {
            const user: UserRegister = {
                email: 'email@email.email',
                username: 'username',
                password: 'password'
            };

            http.post.and.returnValue(of(user));

            const service: AuthService = TestBed.get(AuthService);
            service.register(user).subscribe(
                (res) => {
                    expect(res).toEqual(user);
                }
            );
        });

        it('register should call http.post', () => {
            const user: UserRegister = {
                email: 'email@email.email',
                username: 'username',
                password: 'password'
            };

            const service: AuthService = TestBed.get(AuthService);

            http.post.calls.reset();

            service.register(user).subscribe(
                () => expect(http.post).toHaveBeenCalledTimes(1)
            );

        });
    });

    describe('logout', () => {
        it('logout should call storage.remove 4 times', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.remove.calls.reset();

            service.logout();

            expect(storage.remove).toHaveBeenCalledTimes(4);
        });

        it('logout should change the subject to <null>', () => {
            const service: AuthService = TestBed.get(AuthService);

            service.logout();

            service.user$.subscribe((username) => expect(username).toBe(null));

        });

        it('logout should change isLoggedIn to false', () => {
            const service: AuthService = TestBed.get(AuthService);

            service.logout();

            expect(service.isLoggedIn).toBe(false);

        });

        it('logout should call router.navigate 1 time', () => {
            const service: AuthService = TestBed.get(AuthService);
            router.navigate.calls.reset();

            service.logout();

            expect(router.navigate).toHaveBeenCalledTimes(1);
        });

        it('logout should call router.navigate wit [`/login`]', () => {
            const service: AuthService = TestBed.get(AuthService);
            router.navigate.calls.reset();
            const route = [`/login`];

            service.logout();

            expect(router.navigate).toHaveBeenCalledWith(route);
        });
    });

    describe('reverseToken', () => {
        it('reverseToken should return decoded token', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = 'token';
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue(storage.get());

            const result = service.reverseToken();

            expect(result).toBe('token');
        });

        it('reverseToken should return empty string, when token was not provided', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.get.and.returnValue('');

            const result = service.reverseToken();

            expect(result).toBe('');
        });

        it('reverseToken should call storage.get 1 time', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.get.calls.reset();

            service.reverseToken();

            expect(storage.get).toHaveBeenCalledTimes(1);
        });
    });

    describe('setUserId', () => {
        it('setUserId should set the userId', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { id: 1 };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue(
                token
            );

            service.setUserId();

            expect(storage.set).toHaveBeenCalledTimes(1);
            expect(storage.set).toHaveBeenCalledWith('userId', token.id);
        });
    });

    describe('setAdminStatus', () => {
        it('setAdminStatus should set the adminStatus to 1 when the user is Admin', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { roles: [{ name: 'Admin' }] };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue(token);

            service.setAdminStatus();

            expect(storage.set).toHaveBeenCalledTimes(1);
            expect(storage.set).toHaveBeenCalledWith('adminStatus', '1');
        });

        it('setAdminStatus should set the adminStatus to 0 when the user is not an Admin', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { roles: [{ name: 'NotAdmin' }] };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue(token);

            service.setAdminStatus();

            expect(storage.set).toHaveBeenCalledTimes(1);
            expect(storage.set).toHaveBeenCalledWith('adminStatus', '0');
        });
    });

    describe('isAuthenticated', () => {
        it('isAuthenticated should return false when no token is set', () => {
            const service: AuthService = TestBed.get(AuthService);
            storage.get.and.returnValue('');

            const result = service.isAuthenticated();

            expect(result).toBe(false);
        });

        it('isAuthenticated should return false when the token has expired', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { tokenHasExpired: true };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtHelper.isTokenExpired.and.returnValue(storage.get().tokenHasExpired);

            const result = service.isAuthenticated();

            expect(result).toBe(false);
        });

        it('isAuthenticated should return true when the token is valid', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = { tokenHasExpired: false };
            storage.set.calls.reset();
            storage.get.and.returnValue(
                token
            );
            jwtHelper.isTokenExpired.and.returnValue(storage.get().tokenHasExpired);

            const result = service.isAuthenticated();

            expect(result).toBe(true);
        });
    });

    describe('login', () => {
        it('login should log the user, if correct data is passed', () => {
            const service: AuthService = TestBed.get(AuthService);
            const user: UserLogin = {
                username: 'username',
                password: 'password'
            };
            http.post.and.returnValue(of(user));
            router.navigate.calls.reset();


            service.login(user).subscribe(
                res => {
                    expect(res).toEqual(user);
                    expect(service.isLoggedIn).toBe(true);
                    expect(router.navigate).toHaveBeenCalledTimes(0);
                    expect(service.redirectUrl).toBe(undefined);
                }
            );
        });

        it('login should redirect to posts if successful', () => {
            const service: AuthService = TestBed.get(AuthService);
            const route = [`/posts`];
            service.redirectUrl = 'redirect';
            const user: UserLogin = {
                username: 'username',
                password: 'password'
            };
            http.post.and.returnValue(of(user));
            router.navigate.calls.reset();


            service.login(user).subscribe(
                res => {
                    expect(res).toEqual(user);
                    expect(service.isLoggedIn).toBe(true);
                    expect(router.navigate).toHaveBeenCalledTimes(1);
                    expect(router.navigate).toHaveBeenCalledWith(route);
                    expect(service.redirectUrl).toBe(null);
                }
            );
        });
    });

    describe('getBanstatus', () => {
        it('getBanstatus should return false if the logged user is not banned', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = 'token';
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue({banstatus: {isBanned: false}});

            const result = service.getBanstatus();

            expect(result).toBe(false);
        });

        it('getBanstatus should return true if the logged user is banned', () => {
            const service: AuthService = TestBed.get(AuthService);
            const token = 'token';
            storage.get.and.returnValue(
                token
            );
            jwtService.JWTDecoder.and.returnValue({banstatus: {isBanned: true}});

            const result = service.getBanstatus();

            expect(result).toBe(true);
        });
    });
});
