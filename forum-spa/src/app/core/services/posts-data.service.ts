import {
  PostNoComments
} from '../../models/posts-no-comments';
import {
  PostWithComments
} from '../../models/post-with-comments';
import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  CreatePostModel
} from 'src/app/models/create-post';
import {
  UpdatePostModel
} from 'src/app/models/update-post';

@Injectable()
export class PostsDataServices {
  public constructor(private readonly http: HttpClient) {}

  public getAllPosts(): Observable<PostNoComments[]> {
    return this.http.get<PostNoComments[]>(`http://localhost:3000/posts`);
  }

  public getPost(postId: string): Observable<PostWithComments> {
    return this.http.get<PostWithComments>(`http://localhost:3000/posts/${postId}`);
  }

  public likePost(postId: string): Observable<PostWithComments> {
    return this.http.put<PostWithComments>(`http://localhost:3000/posts/${postId}/likes`, '');
  }

  public dislikePost(postId: string): Observable<PostWithComments> {
    return this.http.put<PostWithComments>(`http://localhost:3000/posts/${postId}/dislikes`, '');
  }

  public createPost(postBody: CreatePostModel): Observable<any> {
    return this.http.post('http://localhost:3000/posts', postBody);
  }

  public flagPost(reason: any, postId: string): Observable<any> {
    return this.http.put<PostWithComments>(`http://localhost:3000/posts/${postId}/flag`, reason);
  }

  public deletePost(postId: string): Observable<any> {
    return this.http.delete(`http://localhost:3000/posts/${postId}`);
  }

  public updatePost(postId: string, updatedPost: UpdatePostModel): Observable<any> {
    return this.http.put(`http://localhost:3000/posts/${postId}`, updatedPost);
  }

  public lockPost(postId: string): Observable<any> {
    return this.http.put(`http://localhost:3000/posts/${postId}/lock`, '');
  }
}
