import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EditCommentModel } from 'src/app/models/edit-comments';
import { CreateCommentModel } from 'src/app/models/create-comment';

@Injectable({
  providedIn: 'root'
})
export class CommentsDataService {
  constructor(private readonly http: HttpClient) { }

  public postComment(postId: string, commentBody: CreateCommentModel): Observable<any> {
    return this.http.post<any>(`http://localhost:3000/posts/${postId}/comments`, commentBody);
  }

  public deleteComment(commentId: string): Observable<any> {
    return this.http.delete(`http://localhost:3000/posts/0/comments/${commentId}`);
  }

  public editComment(commentId: string, commentBody: EditCommentModel): Observable<any> {
    return this.http.put<any>(`http://localhost:3000/posts/0/comments/${commentId}`, {body: commentBody});
  }
}
