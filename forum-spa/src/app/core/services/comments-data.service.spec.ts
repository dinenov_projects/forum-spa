import { HttpClient } from "@angular/common/http";
import { TestBed } from '@angular/core/testing';
import { CommentsDataService } from './comments-data.service';
import { of } from 'rxjs';

describe('CommentsDataService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['post']);

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
        ]
    }));

    it('should be created', () => {
        const service: CommentsDataService = TestBed.get(CommentsDataService);
        expect(service).toBeTruthy();
    });
    it('should create a comment', () => {
        const postId='1'
        const comment={
            body:'test'
        }
        http.post.and.returnValue(of(comment));
        const service: CommentsDataService = TestBed.get(CommentsDataService);
        service.postComment(postId,comment).subscribe(
            (res) => {
                expect(res).toEqual(comment);
            }
        );
    });
})