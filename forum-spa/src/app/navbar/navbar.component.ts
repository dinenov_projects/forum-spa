import {
  Component,
  OnInit,
  OnDestroy,
} from '@angular/core';
import {
  AuthService
} from '../core/services/auth.service';
import {
  NgbModalConfig,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {
  Subscription
} from 'rxjs';
import {
  PostsDataServices
} from '../core/services/posts-data.service';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  public successMessage: any;
  public createPostSubscription: Subscription;
  public isNavbarCollapsed = true;
  public isBanned: boolean;

  constructor(
    private readonly authService: AuthService,
    config: NgbModalConfig,
    private modalService: NgbModal,
    private router: Router,
    private postService: PostsDataServices,
    // private readonly usersDataService: UsersDataService,


  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    if (this.isLoggedIn()) {
      this.isBanned = this.authService.getBanstatus();
    }
  }

  public logout() {
    this.authService.logout();
    this.isBanned = false;
  }

  public reverseToken() {
    const reversed = this.authService.reverseToken();
    return {
      username: reversed.username,
      id: reversed.id,
    };
  }

  ngOnDestroy(): void {
    if (this.createPostSubscription) {
      this.createPostSubscription.unsubscribe();
    }
  }

  isLoggedIn() {
    if (this.authService.isAuthenticated()) {
      this.isBanned = this.authService.getBanstatus();

      return true;
    }
    return false;
  }

  open(content) {
    this.modalService.open(content);
  }
  public createPost(title: string, description: string, body: string) {
    const post = {
      title,
      description,
      body
    };

    this.createPostSubscription = this.postService.createPost(post).subscribe((data) => {
      if (data.message === 'Post has been submitted successfully!') {
        this.successMessage = data.message;
      }
      setTimeout(() => {
        location.reload();
      }, 1500);

      this.router.navigate(['/posts']);
    }, (err: any) => {
      this.successMessage = 'Fill all fields';

    });
  }
}
