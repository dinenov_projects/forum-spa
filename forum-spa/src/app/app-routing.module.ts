import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { RegisterComponent } from './registerform/register/register.component';
import { LoginComponent } from './loginform/login/login.component';
import { AuthGuardService as AuthGuard } from '../../src/app/core/services/auth-guard.service';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { FriendsComponent } from './user/friends/friends.component';
import { ErrorComponent } from './shared/error/error.component';

const routes: Routes = [
  { path: '', redirectTo: '/posts', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'posts', loadChildren: './posts/posts.module#PostsModule', canActivate: [AuthGuard] },
  { path: 'users', loadChildren: './user/user.module#UserModule', canActivate: [AuthGuard] },
  { path: 'friends', component: FriendsComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
